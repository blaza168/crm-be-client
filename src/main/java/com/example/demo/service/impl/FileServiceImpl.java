package com.example.demo.service.impl;

import com.example.demo.FileContent;
import com.example.demo.FileMetadata;
import com.example.demo.model.FileUploadMetadata;
import com.example.demo.model.FileWithBody;
import com.example.demo.remote.ImageClient;
import com.example.demo.service.FileService;
import org.springframework.core.io.ByteArrayResource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class FileServiceImpl implements FileService {

    private final ImageClient imageClient;

    public FileServiceImpl(ImageClient imageClient) {
        this.imageClient = imageClient;
    }

    @Override
    public FileWithBody fileOrDefault(String pCategory, String pKey, String sCategory, String sKey) throws IOException {
        ByteArrayOutputStream content = new ByteArrayOutputStream();
        Iterable<FileContent> responseIterable = () -> imageClient.imageOrDefault(pCategory, pKey, sCategory, sKey);
        return getFileWithBody(responseIterable);
    }

    @Override
    public FileWithBody retrieveFile(Long id) throws IOException {
        ByteArrayOutputStream content = new ByteArrayOutputStream();
        Iterable<FileContent> responseIterable = () -> imageClient.retrieveImage(id);
        return getFileWithBody(responseIterable);
    }

    @Override
    public List<Long> listImages(String category, String key) {
        return imageClient.retrieveImages(category, key).getIdsList();
    }

    private FileWithBody getFileWithBody(Iterable<FileContent> responseIterable) throws IOException {
        Iterator<FileContent> iterator = responseIterable.iterator();
        FileMetadata fileMetadata = iterator.next().getMetadata();

        return new FileWithBody(
                fromFileStorageMetadata(fileMetadata),
                iterator
        );
    }

    private FileUploadMetadata fromFileStorageMetadata(FileMetadata metadata) {
        return new FileUploadMetadata(metadata.getFilename(), metadata.getMimetype());
    }
}
