package com.example.demo.service;

import com.example.demo.model.FileWithBody;

import java.io.IOException;
import java.util.List;

public interface FileService {
    FileWithBody fileOrDefault(String pCategory, String pKey, String sCategory, String sKey) throws IOException;
    FileWithBody retrieveFile(Long id) throws IOException;
    List<Long> listImages(String category, String key);
}
