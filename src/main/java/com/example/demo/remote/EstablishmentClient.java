package com.example.demo.remote;

import com.example.demo.Establishment;
import com.example.demo.EstablishmentEndpointGrpc;
import com.example.demo.ListEstablishmentsMessage;
import com.example.demo.Route;
import com.example.demo.exception.NotFoundException;
import java.util.List;
import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EstablishmentClient {

    private final EstablishmentEndpointGrpc.EstablishmentEndpointBlockingStub blockingStub;

    public EstablishmentClient(ManagedChannel managedChannel) {
        blockingStub = EstablishmentEndpointGrpc.newBlockingStub(managedChannel);
    }

    public Establishment retrieveEstablishment(String route) throws NotFoundException {
        try {
            Route routeMessage = Route.newBuilder()
                    .setRoute(route)
                    .build();

            return blockingStub.retrieveEstablishment(routeMessage);
        } catch (StatusRuntimeException e) {
          if (e.getStatus().getCode() == Status.NOT_FOUND.getCode()) {
              throw new NotFoundException("Establishment not found");
          }
          throw e;
        } catch (Exception e) {
            log.error("Retrieving establishment failed", e);
            throw e;
        }
    }

    public List<Establishment> listEstablishments() {
        try {
            Empty request = Empty.newBuilder().build();

            ListEstablishmentsMessage listEstablishments = blockingStub.listEstablishments(request);

            return listEstablishments.getEstablishmentsList();
        } catch (Exception e) {
            log.error("Listing establishments failed", e);
            throw e;
        }
    }
}
