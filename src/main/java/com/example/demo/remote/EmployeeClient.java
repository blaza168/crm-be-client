package com.example.demo.remote;

import com.example.demo.*;
import com.example.demo.exception.NotFoundException;
import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import java.util.List;

/**
 * Grpc client for working with employees in unibox
 */
@Slf4j
public class EmployeeClient {

    private final EmployeeEndpointGrpc.EmployeeEndpointBlockingStub blockingStub;

    public EmployeeClient(ManagedChannel managedChannel) {
        blockingStub = EmployeeEndpointGrpc.newBlockingStub(managedChannel);
    }

    public Employee retrieveEmployee(String route) throws NotFoundException {
        Route routeMessage = Route.newBuilder()
                .setRoute(route)
                .build();

        try {
            return blockingStub.retrieveEmployee(routeMessage);
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getCode() == Status.Code.NOT_FOUND) {
                log.warn("Employee not found", e);
                throw new NotFoundException(e.getMessage());
            }
            log.error("Unknown error when retrieving employee", e);
            throw e;
        } catch (Exception e) {
            log.error("Retrieving employee failed", e);
            throw e;
        }
    }

    public List<Employee> listEmployees() {
        Empty request = Empty.newBuilder().build();

        try {
            ListEmployeesMessage employeesList = blockingStub.listEmployees(request);
            return employeesList.getEmployeesList();
        } catch (Exception e) {
            log.error("Listing employee failed", e);
            throw e;
        }
    }
}
