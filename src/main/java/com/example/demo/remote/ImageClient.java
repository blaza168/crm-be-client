package com.example.demo.remote;

import com.example.demo.*;
import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;
import java.util.Iterator;


@Slf4j
public class ImageClient {

    private final ImageEndpointGrpc.ImageEndpointStub stub;
    private final ImageEndpointGrpc.ImageEndpointBlockingStub blockingStub;

    public ImageClient(ManagedChannel channel) {
        stub = ImageEndpointGrpc.newStub(channel);
        blockingStub = ImageEndpointGrpc.newBlockingStub(channel);
    }

    public Iterator<FileContent> imageOrDefault(String primaryCategory, String primaryKey, String secondaryCategory, String secondaryKey) {
        ImageIdentificators primaryIdentifications = ImageIdentificators.newBuilder()
                .setCategory(primaryCategory)
                .setKey(primaryKey)
                .build();

        ImageWithDefault.Builder builder = ImageWithDefault.newBuilder()
                .setPrimary(primaryIdentifications);

        if (secondaryCategory != null && secondaryKey != null) {
            ImageIdentificators secondaryIdentifications = ImageIdentificators.newBuilder()
                    .setCategory(secondaryCategory)
                    .setKey(secondaryKey)
                    .build();
            builder.setDefault(secondaryIdentifications);
        }

        return blockingStub.retrieveImageOrDefault(builder.build());
    }

    public RetrieveImagesResponse retrieveImages(String category, String key) {
        ImageIdentificators.Builder builder = ImageIdentificators.newBuilder();
        builder.setCategory(category);
        builder.setKey(key);

        return blockingStub.retrieveImages(builder.build());
    }

    public Iterator<FileContent> retrieveImage(Long id) {
        Id request = Id.newBuilder()
                .setId(id)
                .build();

        return blockingStub.retrieveImage(request);
    }
}
