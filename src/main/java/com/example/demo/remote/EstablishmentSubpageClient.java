package com.example.demo.remote;

import com.example.demo.EstablishmentSubpage;
import com.example.demo.EstablishmentSubpageEndpointGrpc;
import com.example.demo.SubpageIdentifications;
import com.example.demo.exception.NotFoundException;
import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EstablishmentSubpageClient {

    private final EstablishmentSubpageEndpointGrpc.EstablishmentSubpageEndpointBlockingStub blockingStub;

    public EstablishmentSubpageClient(ManagedChannel managedChannel) {
        blockingStub = EstablishmentSubpageEndpointGrpc.newBlockingStub(managedChannel);
    }

    public EstablishmentSubpage retrieveSubpage(Long estId, String subpageRoute) throws NotFoundException {
        try {
            SubpageIdentifications identifications = SubpageIdentifications.newBuilder()
                    .setEstablishmentId(estId)
                    .setSubpageRoute(subpageRoute)
                    .build();

            return blockingStub.retrieveSubpage(identifications);
        } catch (StatusRuntimeException e) {
          if (e.getStatus().getCode() == Status.NOT_FOUND.getCode()) {
              throw new NotFoundException("Establishment not found");
          }
          log.error("Unknown error code when retrieving subpage", e);
          throw e;
        } catch (Exception e) {
            log.error("Unexpected error when retrieving establishment subpage", e);
            throw e;
        }
    }

}
