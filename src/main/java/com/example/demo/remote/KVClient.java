package com.example.demo.remote;

import com.example.demo.Hash;
import com.example.demo.HashRequest;
import com.example.demo.KVStorageEndpointGrpc;
import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KVClient {

    private final KVStorageEndpointGrpc.KVStorageEndpointBlockingStub blockingStub;

    public KVClient(ManagedChannel channel) {
        blockingStub = KVStorageEndpointGrpc.newBlockingStub(channel);
    }

    public Hash retrieve(HashRequest request) {
        return blockingStub.retrieveHash(request);
    }

}
