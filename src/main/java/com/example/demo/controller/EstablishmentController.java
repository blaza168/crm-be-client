package com.example.demo.controller;

import com.example.demo.Establishment;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.EstablishmentResponse;
import com.example.demo.remote.EstablishmentClient;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/crm/establishment")
public class EstablishmentController {

    private final EstablishmentClient establishmentClient;
    private final Converter<Establishment, EstablishmentResponse> converter;

    public EstablishmentController(EstablishmentClient establishmentClient, Converter<Establishment, EstablishmentResponse> converter) {
        this.establishmentClient = establishmentClient;
        this.converter = converter;
    }

    @GetMapping("/{route}")
    public ResponseEntity<Establishment> getEstablishment(@PathVariable String route) {
        try {
            return new ResponseEntity<>(establishmentClient.retrieveEstablishment(route), HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/list")
    public ResponseEntity<List<EstablishmentResponse>> listEstablishments() {
        try {
            List<Establishment> establishments = establishmentClient.listEstablishments();
            return new ResponseEntity<>(establishments.stream().map(this.converter::convert).collect(Collectors.toList()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
