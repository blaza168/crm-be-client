package com.example.demo.controller;

import com.example.demo.Employee;
import com.example.demo.exception.NotFoundException;
import com.example.demo.remote.EmployeeClient;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/crm/employee")
public class EmployeeController {

    private final EmployeeClient employeeClient;

    public EmployeeController(EmployeeClient employeeClient) {
        this.employeeClient = employeeClient;
    }

    @GetMapping("/list")
    public ResponseEntity<List<Employee>> listEmployees() {
        try {
            List<Employee> employees = employeeClient.listEmployees();
            return new ResponseEntity<>(employees, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{route}")
    public ResponseEntity<Employee> retrieveEmployee(@PathVariable String route) {
        try {
            Employee employee = employeeClient.retrieveEmployee(route);
            return new ResponseEntity<>(employee, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
