package com.example.demo.controller;

import com.example.demo.EstablishmentSubpage;
import com.example.demo.exception.NotFoundException;
import com.example.demo.remote.EstablishmentSubpageClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/crm/establishment-subpage")
public class EstablishmentSubpageController {

    private final EstablishmentSubpageClient subpageClient;

    public EstablishmentSubpageController(EstablishmentSubpageClient subpageClient) {
        this.subpageClient = subpageClient;
    }

    @GetMapping("/{establishmentId}/{subpageRoute}")
    public ResponseEntity<EstablishmentSubpage> retrieveSubpage(
            @PathVariable Long establishmentId,
            @PathVariable String subpageRoute
    ) {
        try {
            EstablishmentSubpage subpage = subpageClient.retrieveSubpage(establishmentId, subpageRoute);

            return new ResponseEntity<>(subpage, HttpStatus.OK);
        } catch (NotFoundException e) {
          return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
