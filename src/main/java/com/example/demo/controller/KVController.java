package com.example.demo.controller;

import com.example.demo.Hash;
import com.example.demo.HashRequest;
import com.example.demo.remote.KVClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/crm/kv")
public class KVController {

    private final KVClient kvClient;

    public KVController(KVClient kvClient) {
        this.kvClient = kvClient;
    }

    @GetMapping("/{key}")
    public ResponseEntity<Hash> retrieveHash(@PathVariable String key) {
        try {
            Hash hash = kvClient.retrieve(HashRequest.newBuilder().setCategory(key).build());
            if (hash == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(hash, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
