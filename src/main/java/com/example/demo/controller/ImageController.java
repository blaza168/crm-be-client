package com.example.demo.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

import com.example.demo.FileContent;
import com.example.demo.model.FileWithBody;
import com.example.demo.service.FileService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/crm/image")
public class ImageController {

    private final FileService fileService;

    public ImageController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping(value = "/get-or-default/{category}/{key}")
    public void getImageOrDefault(
            @PathVariable String category,
            @PathVariable String key,
            HttpServletResponse response
    ) throws IOException {
        try {
            FileWithBody fileWithBody = fileService.fileOrDefault(category, key, null, null);

            sendFile(response, fileWithBody);
        } catch (Exception e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            OutputStream os = response.getOutputStream();
            os.flush();
            os.close();
        }
    }

    @GetMapping(value = "/get-or-default/{pCategory}/{pKey}/{sCategory}/{sKey}")
    public void getImageOrDefault(
            @PathVariable String pCategory,
            @PathVariable String pKey,
            @PathVariable String sCategory,
            @PathVariable String sKey,
            HttpServletResponse response
    ) throws IOException {
        try {
            FileWithBody fileWithBody = fileService.fileOrDefault(pCategory, pKey, sCategory, sKey);
            if (fileWithBody == null) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return;
            }

            sendFile(response, fileWithBody);
        } catch (Exception e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            OutputStream os = response.getOutputStream();
            os.flush();
            os.close();
        }
    }

    @GetMapping(value = "/{category}/{key}", produces = "application/json")
    public ResponseEntity<List<Long>> listImages(
            @PathVariable String category,
            @PathVariable String key
    ) {
        try {
            List<Long> ids = fileService.listImages(category, key);
            return new ResponseEntity<>(ids, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{id}")
    public void retrieveImage(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {
        try {
            FileWithBody fileWithBody = fileService.retrieveFile(id);
            if (fileWithBody == null) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return;
            }

            sendFile(response, fileWithBody);
        } catch (Exception e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            OutputStream os = response.getOutputStream();
            os.flush();
            os.close();
        }
    }

    private void sendFile(HttpServletResponse response, FileWithBody fileWithBody) throws IOException {
        response.setStatus(200);
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename\"" + fileWithBody.getMetadata().getFilename() + "\"");
        response.setHeader(HttpHeaders.CONTENT_TYPE, fileWithBody.getMetadata().getMimetype());

        // streaming
        OutputStream os = response.getOutputStream();
        Iterator<FileContent> bodyIterator = fileWithBody.getBody();

        while (bodyIterator.hasNext()) {
            byte[] chunk = bodyIterator.next().getContent().toByteArray();
            os.write(chunk, 0, chunk.length);
        }

        os.flush();
        os.close();
    }
}
