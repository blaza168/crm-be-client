package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class EstablishmentSubpageResponse {

    private final String name;

    private final String route;

    private final String content;

    public EstablishmentSubpageResponse(
            @JsonProperty("name") String name,
            @JsonProperty("route") String route,
            @JsonProperty("content") String content
    ) {
        this.name = name;
        this.route = route;
        this.content = content;
    }
}
