package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

@Getter
public class EstablishmentResponse {

    @JsonProperty("id")
    private final Long id;
    @JsonProperty("name")
    private final String name;
    @JsonProperty("route")
    private final String route;
    @JsonProperty("subpages")
    private final List<EstablishmentSubpageResponse> subpages;

    public EstablishmentResponse (
            Long id,
            String name,
            String route,
            List<EstablishmentSubpageResponse> subpages
    ) {
        this.id = id;
        this.name = name;
        this.route = route;
        this.subpages = subpages;
    }
}
