package com.example.demo.model;

import com.example.demo.FileContent;
import lombok.Getter;
import org.springframework.core.io.Resource;

import java.util.Iterator;

@Getter
public class FileWithBody {

    private final FileUploadMetadata metadata;

    private final Iterator<FileContent> body;

    public FileWithBody(FileUploadMetadata metadata, Iterator<FileContent> body) {
        this.metadata = metadata;
        this.body = body;
    }
}
