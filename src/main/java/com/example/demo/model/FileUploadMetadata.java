package com.example.demo.model;

import lombok.Getter;

@Getter
public class FileUploadMetadata {

    private final String filename;

    private final String mimetype;

    public FileUploadMetadata(String filename, String mimetype) {
        this.filename = filename;
        this.mimetype = mimetype;
    }
}
