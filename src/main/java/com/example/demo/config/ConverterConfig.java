package com.example.demo.config;

import com.example.demo.Establishment;
import com.example.demo.converter.EstablishmentToResponseConverter;
import com.example.demo.model.EstablishmentResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

@Configuration
public class ConverterConfig {

    @Bean
    public Converter<Establishment, EstablishmentResponse> establishmentToResponseConverter() {
        return new EstablishmentToResponseConverter();
    }
}
