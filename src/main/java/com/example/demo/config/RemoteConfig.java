package com.example.demo.config;

import com.example.demo.remote.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RemoteConfig {

    @Bean(name = "filesManagedChannel")
    public ManagedChannel fileManagedChannel (
            @Value("${remote.files.host}") String host,
            @Value("${remote.files.port}") int port
    ) {
        return ManagedChannelBuilder.forAddress(host, port).usePlaintext(true).build();
    }

    @Bean(name = "uniboxManagedChannel")
    public ManagedChannel uniboxManagedChannel (
            @Value("${remote.unibox.host}") String host,
            @Value("${remote.unibox.port}") int port
    ) {
        return ManagedChannelBuilder.forAddress(host, port).usePlaintext(true).build();
    }

    @Bean
    public ImageClient imageClient(@Qualifier("filesManagedChannel") ManagedChannel channel) {
        return new ImageClient(channel);
    }

    @Bean
    public EmployeeClient employeeClient(@Qualifier("uniboxManagedChannel") ManagedChannel channel) {
        return new EmployeeClient(channel);
    }

    @Bean
    public EstablishmentSubpageClient establishmentSubpageClient(@Qualifier("uniboxManagedChannel") ManagedChannel managedChannel) {
        return new EstablishmentSubpageClient(managedChannel);
    }

    @Bean
    public EstablishmentClient establishmentClient(@Qualifier("uniboxManagedChannel") ManagedChannel managedChannel) {
        return new EstablishmentClient(managedChannel);
    }

    @Bean
    public KVClient kvClient(@Qualifier("uniboxManagedChannel") ManagedChannel channel) {
        return new KVClient(channel);
    }
}
