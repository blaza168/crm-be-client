package com.example.demo.config;

import com.example.demo.remote.ImageClient;
import com.example.demo.service.FileService;
import com.example.demo.service.impl.FileServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfig {

    @Bean
    public FileService fileService(ImageClient imageClient) {
        return new FileServiceImpl(imageClient);
    }

}
