package com.example.demo.converter;

import com.example.demo.Establishment;
import com.example.demo.EstablishmentSubpage;
import com.example.demo.model.EstablishmentResponse;
import com.example.demo.model.EstablishmentSubpageResponse;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

public class EstablishmentToResponseConverter implements Converter<Establishment, EstablishmentResponse> {
    @Override
    public EstablishmentResponse convert(Establishment source) {

        List<EstablishmentSubpageResponse> subpageList = new ArrayList<>();

        for (EstablishmentSubpage subpage: source.getSubpagesList()) {
            subpageList.add(new EstablishmentSubpageResponse(subpage.getName(), subpage.getRoute(), subpage.getContent()));
        }

        return new EstablishmentResponse(
                source.getId(),
                source.getName(),
                source.getRoute(),
                subpageList
        );
    }
}
